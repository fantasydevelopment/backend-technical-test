# Fantasy Bet Backend Technical Test #

### Match Data Service

To integrate new sports going forward, fantasybet.com will be utilising a new real-time match data service – `realtimematches.com`. The first sports feed we will be integrating is **football**.

realtimematches.com will send periodic `POST` requests to our new application during the lifetime of a match. The request will contain match information and a link to an associated XML file. The XML file will include the match stats for each player at that given time.

### Requirements

You have been tasked with developing a server-side application that will:

* Accept the above `POST` request.
* Create a "Match" entity and aggregate the data within the XML files.
* The data must be persisted to a database (MySQL, SQLite, PostgreSQL).
* You will find example data within the `feeds > sports > football` directory.
* Provide a simple REST service capable of returning the match data in JSON format – a JSON payload has been provided below.
* You may develop with PHP or Go (please let us know if you would prefer to use another language before beginning).
* Tests are important and will be required to show proof of work.
* Any framework, methodology or application structures are welcome.

The fields that we want to aggregate are:

* Top scorer
* Winner
* Total goals
* Total red cards
* Total yellow cards
* Home team total tackles
* Home team total touches
* Home team total fouls    
* Away team total tackles
* Away team total touches
* Away team total fouls    

##### Extra credit

* Additional filters within the REST service. For example, return all games where there were more than 4 goals etc.
* Return all matches for a team

### Example requests

##### The `POST` request from realtimematches.com

```
POST  HTTP/1.1
Host: localhost:3000
Content-Type: application/json
Cache-Control: no-cache
Postman-Token: 56db581d-85af-588c-7796-01baea35a39b

{
  "competition": "English Barclays Premier League",
  "match_id": 2723,
  "season": 2017,
  "sport": "football",
  "teams": {
	"home": "Arsenal",
	"away": "Leicester City"
  },
  "created_at": "2018-01-08 15:24:20",
  "updated_at": "2018-01-08 15:24:20",
  "feed_file": "https://realtimematches.com/feed/football/2723-489337.xml"
}
```

#### JSON Response

Using the game from `POST` request example, the REST Service would return something like the following:

https://fantasybet.com/api/match-data/football/2723

```json
{
  "competition": "English Barclays Premier League",
  "match_id": "2723",
  "season": "2017",
  "sport": "Football",
  "teams": {
	"home": "Arsenal",
	"away": "Leicester City"
  },
  "match_length": 96, // Minutes
  "match_date": "2017-08-11 :19:46:03",
  "created_at": "2018-01-08 15:24:20",
  "updated_at": "2018-01-08 15:24:20",
  "complete": true,
  "stats": {
    "top_scorer": string,
    "winner": string,
    "total_goals": integer,
    "red_cards": integer,
    "yellow_cards": integer,
    "home": {
      "total_tackles": integer,
      "total_touches": integer,
      "total_fouls": integer
    },
    "away": {
      "total_tackles": integer,
      "total_touches": integer,
      "total_fouls": integer
    },
  }
}
```

## Things to note

* realtimematches.com is a fake domain.
* Code structure, reusability, testing and coding standards are a few important things to consider when developing your solution.